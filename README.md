# [elijahlove.xyz](https://elijahlove.xyz)

Terraform project and other supporting files for my [personal website](https://gitlab.com/elijahdl/elijahlove-hugo)

[Read about it on my blog](https://elijahlove.xyz/blog/2022-10-02_once-again-from-the-top/)

## Terraform
If you clone or fork this project you'll need to create a `tfvars` file or supply input variables in another [supported way](https://www.terraform.io/language/values/variables).

The Terraform configuration creates two DigitalOcean App instances, one for a master branch of the static site git repo and one for a WIP branch of the static site git repo. At the time of this writing, DO lets you host 3 static site Apps for free.
