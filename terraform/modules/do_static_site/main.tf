resource "digitalocean_app" "static_site" {
  spec {
    domains = [
      var.base_domain,
    ]
    name   = var.site_name
    region = "nyc"

    alert {
      disabled = false
      rule     = "DEPLOYMENT_FAILED"
    }

    static_site {
      build_command    = var.build_command
      environment_slug = "hugo"
      name             = var.site_name
      source_dir       = "/"
      error_document   = "404.html"

      env {
        key   = "HUGO_VERSION"
        scope = "BUILD_TIME"
        value = var.hugo_version
      }

      env {
        key   = "HUGO_EXTENDED"
        scope = "BUILD_TIME"
        value = 1
      }

      gitlab {
        branch         = var.branch
        deploy_on_push = true
        repo           = var.site_repo
      }

      routes {
        path                 = "/"
        preserve_path_prefix = false
      }
    }
  }
}