output "url" {
  description = "Static Site URL"
  value       = "https://${var.base_domain}"
}