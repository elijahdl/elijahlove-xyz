variable "do_token" {
  description = "DigitalOcean Access Token"
}

variable "site_name" {
  default     = "elijahlove-xyz"
  description = "The name of the website"
}

variable "base_domain" {
  default     = "elijahlove.xyz"
  description = "Base domain of the website"
}

variable "site_repo" {
  default     = "https://gitlab.com/elijahdl/elijahlove-hugo"
  description = "git repo of site source"
}

variable "branch" {
  default     = "master"
  description = "Branch in repo to use"
}

variable "build_command" {
  default     = "hugo -d public"
  description = "Command to build website"
}

variable "hugo_version" {
  default     = "0.92.2"
  description = "Version of hugo to use"
}