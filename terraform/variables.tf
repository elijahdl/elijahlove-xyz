variable "do_token" {
  description = "DigitalOcean Access Token"
}

variable "site_name" {
  default     = "elijahlove-xyz"
  description = "The name of the website"
}

variable "base_domain" {
  default     = "elijahlove.xyz"
  description = "Base domain of the website"
}

variable "site_repo" {
  default     = "https://gitlab.com/elijahdl/elijahlove-hugo"
  description = "git repo of site source"
}

variable "prod_branch" {
  default     = "master"
  description = "Production branch in repo to use"
}

variable "develop_branch" {
  default     = "develop"
  description = "Development branch in repo to use"
}

variable "build_command" {
  default     = "hugo -d public"
  description = "Command to build website"
}

variable "hugo_version" {
  default     = "0.92.2"
  description = "Version of hugo to use"
}

variable "prod_name" {
  default  = "prod"
}

variable "develop_name" {
  default = "develop"
}