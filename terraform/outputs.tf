output "prod_url" {
  description = "Production Site URL"
  value       = "https://${var.base_domain}"
}

output "develop_url" {
  description = "Development Site URL"
  value       = "https://develop.${var.base_domain}"
}