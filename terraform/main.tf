module "static_site" {
  count = 2
  source = "./modules/do_static_site"
  site_name = "${var.site_name}-${[var.prod_name, var.develop_name][count.index]}"
  base_domain = [var.base_domain, "${var.develop_name}.${var.base_domain}"][count.index]
  site_repo = var.site_repo
  branch = [var.prod_branch, var.develop_branch][count.index]
  build_command = var.build_command
  hugo_version = var.hugo_version
  do_token = var.do_token
}
