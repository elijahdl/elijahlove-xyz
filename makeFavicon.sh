#!/bin/bash

# This script fishes out a couple of variables from $CSS_FILE
# and generates favicons in $STATIC_FILES_DIR
# Colors should be in hexadecimal format
# This script currently can't tell commented and uncommented CSS apart

set -e

STATIC_FILES_DIR="./elijahlove-hugo/static"
CSS_FILE="./elijahlove-hugo/themes/resume-focus-theme/static/css/style.css"

get-css-variable () {
	grep -e ".*$1: .*;" $CSS_FILE | \
		sed  "s/.*$1: \(.*\);/\1/p" | \
		head -n 1
}

COLOR1=`get-css-variable "--accent-1-color"`
COLOR2=`get-css-variable "--accent-2-color"`

echo "Creating favicons with colors $COLOR1 and $COLOR2"

SIZES=("16" "32" "64" "128")

mkdir -p $STATIC_FILES_DIR

convert -size 256x256 gradient:$COLOR2-$COLOR1 -distort SRT 45 \( -size 256x256 \
           xc:Black \
           -fill White \
           -draw 'circle 100 100 100 1' \
           -alpha Copy \
        \) -compose CopyOpacity -composite \
        -trim ${STATIC_FILES_DIR}/favicon-256.png

for size in "${SIZES[@]}"; do
	convert ${STATIC_FILES_DIR}/favicon-256.png \
		-resize ${size}x${size} \
		${STATIC_FILES_DIR}/favicon-${size}.png
done

convert \
	${STATIC_FILES_DIR}/favicon-16.png \
	${STATIC_FILES_DIR}/favicon-32.png \
	${STATIC_FILES_DIR}/favicon-64.png \
	${STATIC_FILES_DIR}/favicon-128.png \
	${STATIC_FILES_DIR}/favicon-256.png \
	${STATIC_FILES_DIR}/favicon.ico
